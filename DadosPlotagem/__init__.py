import numpy as np


def dados_plot(A, Fin, Fs, tempo):
    W = Fin * (2 * np.pi)  # Frequencia angular
    ts = np.linspace(0, tempo, Fs)  # (inicio, fim, passo) ; Faixa de valores do tempo
    y = A * (np.cos(ts * W))  # Função Cossenoidal do sinal
    print(f'y = {y}')
    return y, ts


# PLOTAGEM DO SINAL RESULTANTE - SOMA
def dados_plot_resultante(y1, y2, freq_out, tempo, amp, desl):
    # SOMA DOS SINAIS
    tamanho = int(len(y1))
    soma = []
    y = []
    for i in range(0, tamanho):
        soma.append(0)
    print(f'soma zerada = {soma}')

    for j in range(0, tamanho):
        soma[j] = y1[j] + y2[j]
    print(f'soma y = {soma}')

    # AMPLIFICAÇÃO
    if amp > 1:
        for l in range(0, tamanho):
            y.append(0)
        for k in range(0, tamanho):
            y[k] = amp * soma[k]  # Sinal Resultante
    else:
        y = soma  # Sinal Resultante

    # DESLOCAMENTO
    x = []
    indice = int(len(y2))
    if desl > 0:    # ATRASO DO SINAL
        for i in range(0, desl):
            x.insert(i, 0)

        cont = 0
        for j in range(desl, indice):
            x.insert(j, y[cont])
            cont += 1
        var_controle = indice-desl
        if cont == var_controle:
            cont = 0
        Yr = (x)

    elif desl < 0:  # ADIANTAMENTO DO SINAL
        decres = (-1)*desl
        difer = indice - decres
        cont = decres
        for j in range(0, difer):
            x.insert(j, y[cont])
            cont += 1
        var_controle = difer + decres
        if cont == var_controle:
            cont = 0

        for i in range(0, decres):
            x.append(0)
        Yr = (x)

    else:   # DESLOCAMENTO NULO
        Yr = (y)

    print(f'YR = {Yr}')
    Ts = np.linspace(0, tempo, freq_out)
    return Yr, Ts
